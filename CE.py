# -*- coding: utf-8 -*-


import os, sys,random,subprocess,re
from pylab import *

class point: 

	def __init__(self,C,x,y):
		self.C = C
		self.x = x
		self.y = y
		
		if not C.est_un_point(x,y):
			print("Ce n'est pas un point de la courbe")
			sys.exit()

	def symetrique(self):
		"""renvoie le symétrique d'un point par rapport à l'axe des abscisse"""
		A.x = self.x
		A.y = - self.y
		A.C = self.C
		return A

	def egal(self,A,B):
		if (A.x == B.x) & (A.y==B.y):
			return(1)
		else:
			return(0) 
	#def addition(self,A,B):
		#if egal()


class courbe_elliptique :
	""" corps Fp de caractéristique p premier > 3
	y²=x³+a*x+b, a,b dans le corps"""
	p = 23 #caractéristique = 5
	def __init__(self,a,b):
		self.discriminant = (-16*(4*a**3+27*b**2))%courbe_elliptique.p
		if self.discriminant == 0:
			"""Vérifie si la courbe est non_singulière"""
			print("Erreur:La courbe est singulière")
			sys.exit()
		self.a = a%courbe_elliptique.p
		self.b = b%courbe_elliptique.p

	def aff(self):
		"""afficher l'équation de la courbe"""
		print('y²=x³+',self.a,'*x+',self.b)
	
	def est_un_point(self,x,y): 
		"""si le point (x,y) appartient à la courbe"""
		if (y**2)%courbe_elliptique.p == (x**3+self.a*x+self.b)%courbe_elliptique.p :
			return(1)
		else:
			return(0)
		
class infini(point):
	def symetrique(self):
		return self

CE = courbe_elliptique(1,1)
CE.aff()
A = point(CE,5,19)

#CE.aff()

